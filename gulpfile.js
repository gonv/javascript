/*
TO DO:

ESLINT need to be configured

ACTIVATE CACHEBUST

*/

// Initialize modules
// Importing specific gulp API functions lets us write them below as series() instead of gulp.series()
const { watch, series, parallel, src, dest } = require('gulp');

// Importing all the Gulp-related packages we want to use
const sass = require('gulp-sass');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');
const babel = require('gulp-babel');
const browsersync = require('browser-sync').create();
// const eslint = require('eslint');
//var replace = require('gulp-replace');
//const sourcemaps = require('gulp-sourcemaps'); 


//FILE PATHS
const base = './src/';
const dist = './dist/';

const src_files = { 
    public: base + '*.html',
    styles: base + 'styles/**/*.scss',
    scripts: base + 'scripts/*.js'
}

const dist_files = {
    public: dist + '',
    styles: dist + 'styles/',
    scripts: dist + 'scripts/'
}

//Public TASK
function publicTask() {
    return src(src_files.public)
        .pipe(dest(dist_files.public))
        .pipe(browsersync.stream());
}

//CSS TASK: concatenates and minifies JS files. 
function stylesTask(){    
    return src(src_files.styles)
        //.pipe(sourcemaps.init()) // initialize sourcemaps first
        .pipe(sass().on('error', sass.logError)) // compile SCSS to CSS
        .pipe(postcss([ autoprefixer(), cssnano() ])) // PostCSS plugins
        //.pipe(sourcemaps.write('.')) // write sourcemaps file in current directory
        .pipe(dest(dist_files.styles))// put final CSS in dist folder
        .pipe(browsersync.stream());
         
}

// JS task: concatenates and uglifies JS files. 
//concat('main.js) ON/OFF = Single/Multi file output 
function scriptsTask(){
    return src([
        src_files.scripts
        //,'!' + 'includes/js/jquery.min.js', // to exclude any specific files
        ])
        .pipe(babel({
            presets: ['@babel/preset-env']
        }))                               
        //.pipe(concat('main.js'))
        .pipe(uglify().on('error', console.error))
        .pipe(dest(dist_files.scripts))
        .pipe(browsersync.stream());
}

// // Cachebust
// function cacheBustTask(){
//     var cbString = new Date().getTime();
//     return src([src_files.public])
//         .pipe(replace(/cb=\d+/g, 'cb=' + cbString))
//         .pipe(dest(dist_files.public));
// }



// function lintTask() {
//     return src([src_files.scripts, './gulpfile.js'])
//         .pipe(eslint())
//         .pipe(eslint.format())
//         .pipe(eslint.failAfterError());
        
// }

// Watch task: watch html, SCSS and JS files for changes
// If any change, run scss and js tasks simultaneously
function watchTask(){
    browsersync.init(null, {
        browser: ["firefox",],
        //proxy: '127.0.0.1:8889',
        server: {
            baseDir: "./dist"
        },
        port: 8080,
        open: 'external',
        reloadOnRestart: true,
        notify: true,
    });

    watch(src_files.public,series(parallel(publicTask)))
        .on('change', browsersync.reload);

    watch(src_files.styles,series(parallel(stylesTask)));

    watch(src_files.scripts,series(parallel(scriptsTask)))
        .on('change', browsersync.reload);     
}

// Export the default Gulp task so it can be run
// Runs the scss and js tasks simultaneously
// then runs cacheBust, then watch task
exports.default = series(
    parallel(publicTask, stylesTask, scriptsTask), 
    //cacheBustTask,
    watchTask
);