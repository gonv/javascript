document.addEventListener('DOMContentLoaded', function (){
    const elModal = document.querySelector('.modal');
    const elOpenModal = document.querySelector('.js-open-modal');
    const elCloseModal = document.querySelector('.js-close-modal');

    function openModal() {
        // elModal.style.display = 'flex';
        elModal.classList.add('active');
    }

    function closeModal() {
        // elModal.style.display = 'none';
        elModal.classList.remove('active');

    }


    elOpenModal.onclick = function () {
        openModal();
    };
    
    elCloseModal.onclick = function () {
        closeModal();
    };

    window.onkeydown = function (esc) {
        //console.log('key pressed');
         //console.log(esc);
        if (esc.keyCode === 27) {
            closeModal();
        }
    };


    // encolhe o header
    window.onscroll = function () {
        //console.log(window.pageYOffset);
        let y = window.pageYOffset;
        const elHeader = document.querySelector('header');
        //elHeader.classList.toggle('small');
        
        if (y >= 100 ) {
            elHeader.classList.add('small');
        } else {
            elHeader.classList.remove('small');
        }
    };

//controla o hamburguer / menu

    const elToggle = document.querySelector('.toggle svg');
    const elMenu = document.querySelector('.menu');
    // const hamTop = document.querySelector('.top');
    //const hamMiddle = document.querySelector('.middle');
    // const hamBottom = document.querySelector('.bottom');
    elToggle.onclick = function() {
        elMenu.classList.toggle('active');
        // hamMiddle.classList.toggle('is-hidden');
    };
});









