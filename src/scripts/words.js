$(function () {
    let dictionary = [
      {
        word: 'carpenter',
        description: 'Works with wood',
      },
      {
        word: 'programmer',
        description: 'Builds software for computers',
      },
      {
        word: 'therapist',
        description:
          'A person who specializes in any of various other medical or psychological therapies',
      },
      {
        word: 'teacher',
        description: 'A person who helps people to learn',
      },
      {
        word: 'influencer',
        description:
          'Someone in your niche or industry with sway over your target audience',
      },
      {
        word: 'youtuber',
        description:
          'A type of videographer who produces videos for the video-sharing website YouTube',
      },
      {
        word: 'politician',
        description:
          'A person who is professionally involved in politics especially as a holder of an elected office',
      },
      {
        word: 'firefighter',
        description:
          'A rescuer extensively trained in firefighting primarily to extinguish hazardous fires',
      },
      {
        word: 'fisherman',
        description: 'A person who catches fish for a living or for sport.',
      },
      {
        word: 'astronaut',
        description:
          'A person trained by a human spaceflight program to command pilot or serve as a crew member of a spacecraft.',
      },
    ];
    //console.log(dictionary);
  
    function getRandom() {
      let rand = Math.floor(Math.random() * Math.floor(dictionary.length));
      return rand;
    }
  
    let random = getRandom();
    let word = dictionary[random].word;
    let n = word.length;
    let description = dictionary[random].description;
    let elWord = $('.word');
    let elDesc = $('.description');
  
    //mostra uma 20 a 80% (random) dos caracteres da palavra
    let nVisible = Math.floor(n * (Math.random() * (60 - 20) + 20) / 100);
  
  
    const visible = [];
  
    for (let x = 0; x < nVisible; x++){
      visible.push(word[Math.floor(Math.random() * (n - 1) + 1)]);
    }
    console.log(visible);
  
  
    for (let i = 0; i < n; i++) {
      //let content = '<div contenteditable="true"></div>';
      //let content = '<div contenteditable="true">' + word[i] + '</div>';
      if (visible.includes(word[i])) {
        elWord.append('<div>' + word[i] + '</div>');
      } else {
        elWord.append('<div contenteditable="true"></div>');
      }
    }
    elDesc.html('<h1>' + description + '</h1>');
  });