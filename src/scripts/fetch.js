let userNum = document.querySelector('.users h1');
let userContainer = document.querySelector('.user-container');
let base = 'https://reqres.in/api/';

let url = base + 'users';
let options = {
  method: 'GET'
};

fetch(url, options)
  .then(response => response.json())
  .then(json => listUsers(json.data));

fetch(url, {
  method: 'PUT',
  body: JSON.stringify({
    name: 'morpheus',
    job: 'leader'
  }),
  headers: {
    "Content-type": "application/json; charset=UTF-8"
  }
})

.then(response => response.json())
.then(json => console.log(json));


function listUsers(data) {
  
  userNum.append(' (' + data.length + ')');
  
  var ordered = [];
  data.forEach(function(item) {
    ordered.push(item.first_name);
  });
  
  ordered.sort();
  //console.log(ordered)
  
  
  /*
  data.sort(function(a, b){
    if(a.first_name < b.first_name) { return -1; }
    if(a.first_name > b.first_name) { return 1; }
    return 0;
  })
  */
  
  ordered.forEach(function(item) {
    //console.log(item);
    //let html = '<div>' + item.first_name + '</div>';
    let html = '<div>' + item + '</div>';
    userContainer.innerHTML += html;
  });
  
  let users = document.querySelectorAll('.user-container div');
  users.forEach(function(item) {
    item.onclick = function (e) {
       console.log(item);

    }
  });
}


