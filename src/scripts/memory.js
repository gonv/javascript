let level = prompt('Qual o nivel de dificuldade? (4, 6 ou 8)');
let container = document.querySelector('.memory');
let flipped = [];
let done = [];

function getColor() {
    let color = '#';
    let letters= '0123456789abcdef';
    for (let i=0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

function shuffle(array) {
    array.sort(() => Math.random() - 0.5);
}

function addToHTML (item) {
    container.innerHTML += item;
}

function hideCards() {
    let els = document.querySelectorAll('.on');
    for (let i = 0; i < els.length; i++) {
        if (!done.includes(els[i].dataset.color)) {
            els[i].classList.toggle('on');
        }
        flipped = [];
    }
}

if (level == 4 || level == 6 || level == 8 ) {


    let nCards = level * level;
    let cardsHTML = [];

    for(nCards = nCards / 2; nCards >= 1; nCards--) {
        let cardColor = getColor();
        cardsHTML.push(
            '<div style=width:' + 100 / level + 
            '%;><div data-color="' + cardColor + '" class="card" style="background: linear-gradient(black, ' + cardColor + ')"></div></div>');
    }

let allcards = cardsHTML.concat(cardsHTML);
shuffle(allcards);
    
allcards.forEach(addToHTML);


let cards = document.querySelectorAll('.card');
for(let i = 0; i < cards.length; i++) {
    cards[i].onclick = function() {
        
        if (flipped.length < 2) {
        this.classList.toggle('on');
        flipped.push(this.dataset.color);
        }

        if (flipped.length === 2) {
            if (flipped[0] === flipped[1]) {
                done.push(flipped[0]);
                flipped = [];
            } else {
                setTimeout(function () {
                    hideCards();
                }, 1000)
            }
        }       
    }
}



} else {
    alert('Escolha um dos niveis indicados!');
}

